import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

//membuat page baru bernama HomePage
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nameC = TextEditingController();
  TextEditingController jobC = TextEditingController();
  String hasilrespon = 'DATA MASIH KOSONG';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HTTP POST'),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: nameC,
              autocorrect: false,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), label: Text('name')),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: jobC,
              autocorrect: false,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), label: Text('job')),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
                onPressed: () async {
                  var Myhttp = await http.post(
                      Uri.parse('https://reqres.in/api/users'),
                      body: {'name': nameC.text, 'job': jobC.text});

                  setState(() {
                    Map<String, dynamic> data =
                        json.decode(Myhttp.body) as Map<String, dynamic>;
                    hasilrespon = '${data['name']} - ${data['job']}';
                  });
                },
                child: Text('SUBMIT')),
          ),
          SizedBox(
            height: 20,
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 15,
          ),
          Center(
            child: Text(
              hasilrespon,
              style: TextStyle(fontSize: 20),
            ),
          )
        ],
      ),
    );
  }
}
